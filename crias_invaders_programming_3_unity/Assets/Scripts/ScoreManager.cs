using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField inputName;
    [SerializeField] private TextMeshProUGUI inputScore;

    public UnityEvent<string, int> submitScoreEvent;

    private void Start()
    {
        GameManager.Instance.OnScoreChange += UpdateScoreUI; // Inscreve o m�todo UpdateScoreUI no evento OnScoreChange
        UpdateScoreUI(GameManager.Instance.Score); // Atualiza o score UI ao iniciar
    }

    public void SubmitScore()
    {
        submitScoreEvent.Invoke(inputName.text, GameManager.Instance.Score); // Envia o score atual do GameManager
    }

    private void UpdateScoreUI(int score)
    {
        inputScore.text = score.ToString(); // Atualiza o campo de entrada de score com o valor atualizado do jogo
    }
}
