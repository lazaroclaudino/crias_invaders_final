using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Unity.VisualScripting;
using System;

public sealed class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    //[SerializeField] private GameObject gameOverUI;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text livesText;


    private Player player;
    private Invaders invaders;
    private MysteryShip mysteryShip;
    private Bunker[] bunkers;
    public AudioSource backgroundSound;

    private int score;
    private int lives;
    private bool playerIsDead;
    private int extraLivesAwarded = 0;

    public int Score => score;
    public int Lives => lives;

    public event Action<int> OnScoreChange; // Defini��o do evento OnScoreChange

    private const int MaxLives = 6;
    private const int ExtraLifeScoreThreshold = 1000;

    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        player = FindObjectOfType<Player>();
        invaders = FindObjectOfType<Invaders>();
        mysteryShip = FindObjectOfType<MysteryShip>();
        bunkers = FindObjectsOfType<Bunker>();
        backgroundSound.Play();

        NewGame();
    }

    private void Update()
    {
        if (lives <= 0 && Input.GetKeyDown(KeyCode.Return))
        {
            NewGame();
        }
    }

    private void NewGame()
    {
        SetScore(0);
        SetLives(3);
        NewRound();
    }

    private void NewRound()
    {
        invaders.ResetInvaders();
        invaders.StartMovement();
        invaders.gameObject.SetActive(true);

        for (int i = 0; i < bunkers.Length; i++)
        {
            bunkers[i].ResetBunker();
        }

        Respawn();
    }

    private void Respawn()
    {
        player.gameObject.SetActive(true);
        Vector3 position = player.transform.position;
        position.x = 0f;
        player.transform.position = position;
    }

    private void GameOver()
    {
        invaders.gameObject.SetActive(false);
        SceneManager.LoadScene("GameOver");
    }

    private void SetScore(int score)
    {
        this.score = score;
        scoreText.text = ("score: " + score.ToString("0"));

        OnScoreChange?.Invoke(score); // Invocando o evento OnScoreChange quando o score muda

        CheckScoreForExtraLife();
    }

    private void SetLives(int lives)
    {
        this.lives = Mathf.Clamp(lives, 0, MaxLives); // Garante que o n�mero de vidas esteja entre 0 e MaxLives
        livesText.text = "lives: " + this.lives.ToString();
    }

    private void CheckScoreForExtraLife()
    {
        int totalLivesAllowed = MaxLives + extraLivesAwarded; // Calcula o n�mero total de vidas permitidas, incluindo as vidas extras j� obtidas
        int extraLives = (int)(score / ExtraLifeScoreThreshold);

        if (extraLives > totalLivesAllowed)
        {
            extraLives = totalLivesAllowed; // Limita o n�mero de vidas extras ao m�ximo permitido
        }

        // Ajuste para permitir recuperar vidas extras perdidas
        if (extraLives > extraLivesAwarded)
        {
            SetLives(lives + (extraLives - extraLivesAwarded));
            extraLivesAwarded = extraLives;
        }
        else if (extraLives < extraLivesAwarded)
        {
            // Se o jogador perdeu vidas extras, permite recuper�-las se a pontua��o aumentar novamente
            extraLivesAwarded = extraLives;
        }
    }

    public void OnPlayerKilled(Player player)
    {
        SetLives(lives - 1);

        player.gameObject.SetActive(false);

        if (lives > 0)
        {
            Invoke(nameof(NewRound), 1f);
        }
        else
        {
            GameOver();
        }
    }

    public void OnInvaderKilled(Invader invader)
    {
        invader.gameObject.SetActive(false);

        SetScore(score + invader.score);

        if (invaders.GetAliveCount() == 0)
        {
            NewRound();
        }
    }

    public void OnMysteryShipKilled(MysteryShip mysteryShip)
    {
        SetScore(score + mysteryShip.score);
    }

    public void OnBoundaryReached()
    {
        if (invaders.gameObject.activeSelf)
        {
            invaders.gameObject.SetActive(false);

            OnPlayerKilled(player);
        }
    }
}
